//
//  MyCell.swift
//  Projeto3Etapa
//
//  Created by COTEMIG on 20/10/22.
//

import UIKit

class MyCell: UITableViewCell {

    @IBOutlet weak var ator: UILabel!
    @IBOutlet weak var personagem: UILabel!
    @IBOutlet weak var imagem: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
