//
//  ViewController.swift
//  Projeto3Etapa
//
//  Created by COTEMIG on 20/10/22.
//

import UIKit
import Alamofire
import Kingfisher

class Personagem: Decodable {
    var name:String
    var actor: String
    var image: String

}

class ViewController: UIViewController, UITableViewDataSource {

    var listaDePersonagens: [Personagem]?

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDePersonagens?.count ?? 0
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celula = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula",for: indexPath) as! MyCell
        let personagemItem = listaDePersonagens?[indexPath.row]
        
        celula.ator.text = personagemItem?.actor
        celula.personagem.text = personagemItem?.name
        if let image = personagemItem?.image {
            celula.imagem.kf.setImage(with:URL(string: image))
        }
        
        return celula
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        getPersonagem()
    }



    func getPersonagem(){
        AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of: [Personagem].self){
            response in
            if let listaDePersonagens =  response.value{
                self.listaDePersonagens = listaDePersonagens
                    
            }
            self.tableView.reloadData()
        }
    }
    
}

